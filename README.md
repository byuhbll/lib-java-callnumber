#Call Number Library
[![Build Status](https://semaphoreci.com/api/v1/projects/66b801d3-9652-4507-9ad1-323721cfdaad/837881/shields_badge.svg)](https://semaphoreci.com/byuhbll/lib-java-callnumber)

The Call Number library provides a flexible but robust way of working with call numbers, officially known as 
[library classifications](https://en.wikipedia.org/wiki/Library_classification).  At the heart of the library is the
`CallNumber` interface and a universal `CallNumberParser`.

*This library requires Java 8.*

##The CallNumber Interface

Classes implementing `CallNumber` normally represent an entire library classification scheme; for example, the library
comes pre-packaged with an `LCCallNumber` implementation (representing the [Library of Congress Classification](https://www.loc.gov/catdir/cpso/lcc.html)) and the
`DeweyCallNumber` implementation (representing the [Dewey Classification](https://www.oclc.org/dewey)).  But this is not always the case, and 
specialized implementation can be created to manage institutional or ILS-specific quirks.  For example, the library also 
includes the `SymphonyDefaultCallNumber` implementation, which allows it to handle the default call number
placeholders used in newly created catalog records in the popular [SirsiDynix Symphony ILS](http://www.sirsidynix.com/products/sirsidynix-symphony).

The `CallNumber` interface requires that implementations define a `sortKey` method.  `sortKey` returns a `String`
that will order/sort correctly (since many library classifications have sorting rules that contradict normal `String`
ordering which is based on the character set).  The `sortKey` response need not be human readable, but it must be 
non-null.

Further, each `CallNumber` implementation should be defined as an immutable, thread-safe value object, similar to 
`String`, `Integer`, or `URI`.  As value objects, implementations are expected to implement a `toString` method that
returns a normalized, human-readable representation of the call number.  Additionally, they should override `hashCode`
and `equals` such that for any two non-null `CallNumber` objects `a` and `b`, if 
`a.getClass().equals(b.getClass())` and `a.sortKey().equals(b.sortKey())`, then `a.equals(b)` is also TRUE.

An additional contractual requirement exists that requires that `CallNumber` implementations must provide a
constructor that accepts a single `String` argument.  This is necessary for the implementation to be suitable as a
target for the `CallNumberParser` described in the next section.

Finally, Users should also be aware that the `CallNumber` interface extends both `Comparable` and `Serializable`, and that
a default implementation for `compareTo` (which compares the `sortKey` response) has been provided which should be 
suitable for most, if not all, implementations.

The following stub implementation may be used as a template for users wishing to create their own `CallNumber` 
implementations:

```
#!java
public final class StubCallNumber {

	private final String sortKey;
	private final String normalization;

	public StubCallNumber(String str) {
		Objects.requireNonNull(str);

		//TODO: Do whatever validation and parsing as is necessary for this implementation...

		normalization = str; //Replace "str" with the human-friendly, normalized version of the call number
		sortKey = str; //Replace "str" with the sortable version of the call number
	}
	
	@Override
	public String sortKey() {
		return sortKey;
	}
	
	@Override
	public String toString() {
		return normalization;
	}
	
	@Override
	public int hashCode() {
		return sortKey.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		
		return Objects.equals(sortKey, ((CallNumber)obj).sortKey());
	}

}
```

##The CallNumberParser Class

The main entry point for most usage of the library will be through the `CallNumberParser` class.  This class is
immutable and thread-safe, and is built by providing a "varargs" array of `CallNumber` implementation classes to the
constructor as parsing target types.  Subsequent `parse` calls to that instance of `CallNumberParser` will attempt 
to parse the provided `String` into the first matching target type, as demonstrated below:

```
#!java

public static void main(String[] args) {
	CallNumberParser p = new CallNumberParser(LCCallNumber.class, DeweyCallNumber.class);

	CallNumber a = p.parse("AB 123");  //Will be an LCCallNumber, since it matches the format for that 
	                                   //implementation, which is the first in the list and thus takes
	                                   //highest priority.

	CallNumber b = p.parse("123.45");  //Will be a DeweyCallNumber, since it failed to match the first 
	                                   //target in the list (LCCallNumber) but matches the format for the
	                                   //second target type (DeweyCallNumber).

	CallNumber c = p.parse("bogus");   //Will throw an IllegalArgumentException, since it matches the 
	                                   //format of neither target type.
}
```

Note that a special `CallNumber` implementation is included with this library, `UnclassifiedCallNumber`, which will
successfully parse against ANY string (including `null` and the empty string).  Users wishing to guarantee that the
`CallNumberParser` will never throw an `IllegalArgumentException` (as demonstrated above) on a non-matching
call number may do so by adding `UnclassifiedCallNumber.class` to the end of their target list when constructing
the `CallNumberParser` instance:

```
#!java

public static void main(String[] args) {
	                                                                                   //Note the addition of this class
	CallNumberParser p = new CallNumberParser(LCCallNumber.class, DeweyCallNumber.class, UnclassifiedCallNumber.class);

	CallNumber c = p.parse("bogus");   //Will return an UnclassifiedCallNumber, since it failed to match
	                                   //any previous target type.  All strings, including null and empty,
	                                   //will successfully parse to an UnclassifiedCallNumber.
}
```

For convenience, we have defined two pre-built `CallNumberParser` instances which may be used via the following
static references:

- `CallNumberParser.SYMPHONY_STRICT`
- `CallNumberParser.SYMPHONY_NONSTRICT`

See the class-level documentation for full details of these pre-built parsers.