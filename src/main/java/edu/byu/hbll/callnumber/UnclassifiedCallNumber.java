package edu.byu.hbll.callnumber;

import java.util.Objects;

/**
 * CallNumber implementation representing an unclassified call number.
 * <p>
 * This is the simplest possible implementation of the CallNumber interface.  It makes no attempt to normalize the
 * source data and merely standardizes case while generating the sortKey.
 */
public final class UnclassifiedCallNumber implements CallNumber {
	
	/**
	 * Serialization version.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The original representation of the call number.
	 */
	private final String normalization;
	
	/**
	 * The sortable representation of the call number.
	 */
	private final String sortKey;
	
	/**
	 * Constructs a new instance using the provided string as the template.
	 * 
	 * @param str the call number string to use as the template.
	 * @throws NullPointerException if a null value is provided
	 * @throws IllegalArgumentException if an empty string is provided
	 */
	public UnclassifiedCallNumber(String str) throws NullPointerException, IllegalArgumentException {
		if(str == null) {
			str = "";
		}
		normalization = str.trim();
		sortKey = normalization.toLowerCase();
	}
	
	@Override
	public String sortKey() {
		return sortKey;
	}
	
	@Override
	public String toString() {
		return normalization;
	}

	@Override
	public int hashCode() {
		return sortKey.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		return Objects.equals(sortKey, ((CallNumber)obj).sortKey());
	}

}
