package edu.byu.hbll.callnumber;

import java.io.Serializable;

/**
 * A call number or shelving key for library materials.
 * <p>
 * Since some call number classification schemes do not always sort according to ASCII/Unicode order, the essential 
 * component of this interface is the sortKey() method.  CallNumber implementations are expected to return a non-null 
 * String from this method which allows for proper comparison and ordering of different call numbers.  This value is not
 * expected to be human-readable.
 * <p>
 * Concrete implementations of CallNumber should be value objects.  They should be final and immutable and should 
 * provide meaningful implementations for equals(Object) and hashCode() based on their value.  Implementations are
 * additionally required to define a public constructor that accepts a single String argument as follows:
 * <p>
 *     public CallNumberImpl(String str) { //... }
 * <p>
 * It is expected that implementations will represent an entire classification of call number such as the Library of 
 * Congress or Dewey classification schemes. Because CallNumbers are value objects based on string values, 
 * implementations of CallNumber should also implement the toString method in such a way that it returns a 
 * human-readable representation of the call number, optionally normalized as appropriate for the classification 
 * represented by that specific implementation.
 * <p>
 * CallNumbers must be comparable to any other CallNumber regardless of implementation, but since sorting rules may be
 * classification-specific, results of the compareTo(CallNumber) method may or may not order CallNumbers as expected 
 * if the two CallNumbers involved do not share the same implementation. A default implementation of compareTo is
 * provided by the interface, which will be suitable for most implementations.
 */
public interface CallNumber extends Serializable, Comparable<CallNumber> {
	
	/**
	 * Returns a String value (sort key) that will properly order this CallNumber with other CallNumbers sharing the 
	 * same implementation. This method must not return a null value.
	 * 
	 * @return the sort key
	 */
	public String sortKey();
	
	@Override
	default public int compareTo(CallNumber other) {
		return sortKey().compareTo(other.sortKey());
	}

}
