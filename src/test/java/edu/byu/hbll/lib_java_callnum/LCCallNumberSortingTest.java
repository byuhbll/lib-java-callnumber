package edu.byu.hbll.lib_java_callnum;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import edu.byu.hbll.callnumber.LCCallNumber;

/**
 * Unit tests for LCCallNumber ordering/sorting.
 */
public class LCCallNumberSortingTest {

	@Test
	public void kentTest1_baseClassification() throws Exception {
		LCCallNumber lower = new LCCallNumber("DB 236 .B3");
		LCCallNumber higher = new LCCallNumber("DD 236 .A65");
		assertTrue(higher.compareTo(lower) > 0);
	}
	
	@Test
	public void kentTest2_nothingBeforeSomething() throws Exception {
		LCCallNumber lower = new LCCallNumber("CD 562 .A96");
		LCCallNumber higher = new LCCallNumber("CD 562 .A961");
		assertTrue(higher.compareTo(lower) > 0);
	}
	
	@Test
	public void kentTest3_baseClassification() throws Exception {
		LCCallNumber lower = new LCCallNumber("ND 473 .D3 1956");
		LCCallNumber higher = new LCCallNumber("NK 473 .D3 cop.2");
		assertTrue(higher.compareTo(lower) > 0);
	}
	
	//Kent test 4 doesn't apply to a general use case

	@Test
	public void kentTest5_cutter1() throws Exception {
		LCCallNumber lower = new LCCallNumber("LB 7968 .M263");
		LCCallNumber higher = new LCCallNumber("LB 7968 .M3");
		assertTrue(higher.compareTo(lower) > 0);
	}

	@Test
	public void kentTest6_combinedCutters() throws Exception {
		LCCallNumber lower = new LCCallNumber("RA 518 .S43A6");
		LCCallNumber higher = new LCCallNumber("RA 518 .S43 B21");
		assertTrue(higher.compareTo(lower) > 0);
	}

	@Test
	public void kentTest7_lettersBeforeNumbers() throws Exception {
		LCCallNumber lower = new LCCallNumber("LC 346 .M634");
		LCCallNumber higher = new LCCallNumber("LC 346 .65 .B29");
		assertTrue(higher.compareTo(lower) > 0);
	}

	@Test
	public void kentTest8_baseClassificationNumber() throws Exception {
		LCCallNumber lower = new LCCallNumber("PT 143 .S41");
		LCCallNumber higher = new LCCallNumber("PT 1421 .B5");
		assertTrue(higher.compareTo(lower) > 0);
	}

	@Test
	public void kentTest9_xAfterNothing() throws Exception {
		LCCallNumber lower = new LCCallNumber("DA 467 .K3");
		LCCallNumber higher = new LCCallNumber("DA 467 .K3x");
		assertTrue(higher.compareTo(lower) > 0);
	}

	@Test
	public void kentTest10_nonDecimalCutter() throws Exception {
		LCCallNumber lower = new LCCallNumber("JK 1978 1789 .L34Z5");
		LCCallNumber higher = new LCCallNumber("JK 1978 .K413x 1722");
		assertTrue(higher.compareTo(lower) > 0);
	}
	
	@Test
	public void kentTest11() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("LC 1085 .2 .B424 1993"));
		expectedOrder.add(new LCCallNumber("LC 1085 .2 .B461 1993"));
		expectedOrder.add(new LCCallNumber("LC 1085 .2 .B63 1994"));
		expectedOrder.add(new LCCallNumber("LC 1085 .2 .L8x 1992"));
		expectedOrder.add(new LCCallNumber("LC 1085 .2 .L88 1993"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void kentTest12() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("LC 1091 .M849 c.3"));
		expectedOrder.add(new LCCallNumber("LC 1099 .M83"));
		expectedOrder.add(new LCCallNumber("LC 1099 .M84x 1988"));
		expectedOrder.add(new LCCallNumber("LC 1099 .M84x C. 2"));
		expectedOrder.add(new LCCallNumber("LC 1099 .M848 1984"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void kentTest13() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("LC 1091 .M67 1989"));
		expectedOrder.add(new LCCallNumber("LC 1091 .M68"));
		expectedOrder.add(new LCCallNumber("LC 1091 .M68 1994"));
		expectedOrder.add(new LCCallNumber("LC 1091 .M68x 1993"));
		expectedOrder.add(new LCCallNumber("LC 1091 .P3x"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void kentTest14() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("RB 73 .N59 1985"));
		expectedOrder.add(new LCCallNumber("RB 73 .O35x"));
		expectedOrder.add(new LCCallNumber("RB 73 .O54 1989"));
		expectedOrder.add(new LCCallNumber("RB 73 .P35 c.2"));
		expectedOrder.add(new LCCallNumber("RB 73 .R62"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void kentTest15() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("GN 568 .C29K5"));
		expectedOrder.add(new LCCallNumber("GN 568 .C29 K9"));
		expectedOrder.add(new LCCallNumber("GN 568 .C3 L15"));
		expectedOrder.add(new LCCallNumber("GN 568 .C32 K7"));
		expectedOrder.add(new LCCallNumber("GN 569 .D5 1969"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void kentTest16() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("Z 767 .B25 P75"));
		expectedOrder.add(new LCCallNumber("Z 767 .M165 1968"));
		expectedOrder.add(new LCCallNumber("Z 767 .5 .B1J5"));
		expectedOrder.add(new LCCallNumber("Z 767 .51 .B1Jx"));
		expectedOrder.add(new LCCallNumber("Z 767 .53 .C45"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void kentTest17() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("ND 6101 .K2"));
		expectedOrder.add(new LCCallNumber("ND 6108 .L454 v.4"));
		expectedOrder.add(new LCCallNumber("ND 6108 .231 .J2"));
		expectedOrder.add(new LCCallNumber("ND 6108 .28 .K3"));
		expectedOrder.add(new LCCallNumber("ND 6108 .3 .Z193"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void kentTest18() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("GT 3062 .R19L3"));
		expectedOrder.add(new LCCallNumber("GT 3062 .R2L2"));
		expectedOrder.add(new LCCallNumber("GT 3062 .R9"));
		expectedOrder.add(new LCCallNumber("GT 3062 .S1"));
		expectedOrder.add(new LCCallNumber("GT 3063 .R19"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void kentTest19() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("S 153 .A19 P6"));
		expectedOrder.add(new LCCallNumber("S 153 .4 .A91C3"));
		expectedOrder.add(new LCCallNumber("S 153.4 .C31"));
		expectedOrder.add(new LCCallNumber("S 153 .4 .P6C44"));
		expectedOrder.add(new LCCallNumber("S 153.4 .P6 H491"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void kentTest20() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("HV 753 .J38 1978"));
		expectedOrder.add(new LCCallNumber("HV 754 .C7723"));
		expectedOrder.add(new LCCallNumber("HV 754 .D98"));
		expectedOrder.add(new LCCallNumber("HX 754 .C15"));
		expectedOrder.add(new LCCallNumber("HX 754 .C9 1963"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}
	
	@Test
	public void kentTest21() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("T 819 .P3 Z24"));
		expectedOrder.add(new LCCallNumber("T 819 .P4W46"));
		expectedOrder.add(new LCCallNumber("T 819 .P4W5"));
		expectedOrder.add(new LCCallNumber("T 819 .P4 Z21"));
		expectedOrder.add(new LCCallNumber("T 819 .W35"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}
	
	@Test
	public void kentTest22() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("PE 143 .A3"));
		expectedOrder.add(new LCCallNumber("PF 143 .B123"));
		expectedOrder.add(new LCCallNumber("PF 144 .C9 c.2"));
		expectedOrder.add(new LCCallNumber("PF 144 .3 G25"));
		expectedOrder.add(new LCCallNumber("PF 144 .45 D3"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void kentTest23() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("JK 1977 .P122"));
		expectedOrder.add(new LCCallNumber("JK 1978 1789 .L34x"));
		expectedOrder.add(new LCCallNumber("JK 1978 .K413 1722"));
		expectedOrder.add(new LCCallNumber("JK 1978 .K413x"));
		expectedOrder.add(new LCCallNumber("JK 1979 .A182"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void kentTest24() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("HQ 72 .A18"));
		expectedOrder.add(new LCCallNumber("HQ 72 .A2"));
		expectedOrder.add(new LCCallNumber("HQ 72 .A2 c.2"));
		expectedOrder.add(new LCCallNumber("HQ 72 .A2 c.2 1988"));
		expectedOrder.add(new LCCallNumber("HQ 73 .A19 G3"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void kentTest25() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("Q 284.2 .L94"));
		expectedOrder.add(new LCCallNumber("Q 284 .2 .R17"));
		expectedOrder.add(new LCCallNumber("Q 284.2 .R2"));
		expectedOrder.add(new LCCallNumber("Q 284.3 .K94"));
		expectedOrder.add(new LCCallNumber("Q 284 .3 .K94x"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void kentTest26() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("HB 102 .B6"));
		expectedOrder.add(new LCCallNumber("HB 103 .B5"));
		expectedOrder.add(new LCCallNumber("HC 100 .B53 A9"));
		expectedOrder.add(new LCCallNumber("HD 97 .A33"));
		expectedOrder.add(new LCCallNumber("HD 101 .B52"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void kentTest27() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("J 133 1948 .J32"));
		expectedOrder.add(new LCCallNumber("J 133 1954 .S42"));
		expectedOrder.add(new LCCallNumber("J 133 1954 .S5"));
		expectedOrder.add(new LCCallNumber("J 133 .S3 cop.2"));
		expectedOrder.add(new LCCallNumber("J 133 .S33 1950"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void kentTest28() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("RC 793 1965 .K75"));
		expectedOrder.add(new LCCallNumber("RC 793 1967 .B73"));
		expectedOrder.add(new LCCallNumber("RC 793 .19 .A8N5"));
		expectedOrder.add(new LCCallNumber("RC 793.2 .A6C4"));
		expectedOrder.add(new LCCallNumber("RC 793 .2 .A7C4 1963"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void kentTest29() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("ND 6108 .M263"));
		expectedOrder.add(new LCCallNumber("ND 6108 .M3"));
		expectedOrder.add(new LCCallNumber("ND 6108 .M3A6"));
		expectedOrder.add(new LCCallNumber("ND 6108 .M3A7"));
		expectedOrder.add(new LCCallNumber("ND 6109 .M5 1999"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void kentTest30() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("QA 71 .R65"));
		expectedOrder.add(new LCCallNumber("QA 76 .H184"));
		expectedOrder.add(new LCCallNumber("QA 76 .H184 cop.2"));
		expectedOrder.add(new LCCallNumber("QA 76 .3 .B2x"));
		expectedOrder.add(new LCCallNumber("QA 76 .3 .B23"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void kentTest31() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("PS 118.6 .B1 1966 cop.2"));
		expectedOrder.add(new LCCallNumber("PS 118 .6 .B19"));
		expectedOrder.add(new LCCallNumber("PS 119 .45 .N93 vol.3"));
		expectedOrder.add(new LCCallNumber("PS 119 .5"));
		expectedOrder.add(new LCCallNumber("PS 119 .51"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void kentTest32() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("ND 893 .D317 1989"));
		expectedOrder.add(new LCCallNumber("ND 894 .B45"));
		expectedOrder.add(new LCCallNumber("NK 894 .A13"));
		expectedOrder.add(new LCCallNumber("NK 894 .C35"));
		expectedOrder.add(new LCCallNumber("NK 894 .1988"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}
	
	@Test
	public void kentTest33() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("BS 497 .A2"));
		expectedOrder.add(new LCCallNumber("BS 497 .A3"));
		expectedOrder.add(new LCCallNumber("BS 497 .A315"));
		expectedOrder.add(new LCCallNumber("BS 497 .A32"));
		expectedOrder.add(new LCCallNumber("BS 497 .B54"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}
	
	/**
	 * Volume/copy numbers should be sorted numerically
	 * 
	 * @throws Exception if an exception is thrown during the test
	 */
	@Test
	public void byuTest1_volumeNumbers() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("Z 7164 .G7 N54 1972 vol.1"));
		expectedOrder.add(new LCCallNumber("Z 7164 .G7 N54 1972 vol.2"));
		expectedOrder.add(new LCCallNumber("Z 7164 .G7 N54 1972 vol.10"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	/**
	 * An "x" in a cutter should sort before a number in the same position
	 * 
	 * @throws Exception if an exception is thrown during the test
	 */
	@Test
	public void byuTest2_xInFirstCutter() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("BF 637 .C6 vol.24"));
		expectedOrder.add(new LCCallNumber("BF 637 .C6x vol.24"));
		expectedOrder.add(new LCCallNumber("BF 637 .C66 vol.24"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}
	
	/**
	 * An "x" in a cutter should sort before a number in the same position
	 * 
	 * @throws Exception if an exception is thrown during the test
	 */
	@Test
	public void byuTest3_xInSecondCutter() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("BF 637 .C6 A88 vol.24"));
		expectedOrder.add(new LCCallNumber("BF 637 .C6 A88x vol.24"));
		expectedOrder.add(new LCCallNumber("BF 637 .C6 A888 vol.24"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	/**
	 * An "x" in a cutter should sort before a number in the same position
	 * 
	 * @throws Exception if an exception is thrown during the test
	 */
	@Test
	public void byuTest4_xInThirdCutter() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("BF 637 .C6 A88 B15 vol.24"));
		expectedOrder.add(new LCCallNumber("BF 637 .C6 A88 B15x vol.24"));
		expectedOrder.add(new LCCallNumber("BF 637 .C6 A88 B155 vol.24"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	/**
	 * Letters may follow a cutter number to provide additional specificity and should be
	 * sorted before a number in the same position
	 * 
	 * @throws Exception if an exception is thrown during the test
	 */
	@Test
	public void byuTest5_lettersAfterCutterNumber() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("BT73.K8D8 1950 v.2"));
		expectedOrder.add(new LCCallNumber("BT73.K8DD 1950 v.2"));
		expectedOrder.add(new LCCallNumber("BT73.K88D 1950 v.2"));
		expectedOrder.add(new LCCallNumber("BT73.K888 1950 v.2"));

		TestUtil.assertCorrectOrder(expectedOrder);
	}
	
	/**
	 * Spaces don't matter to sorting
	 * 
	 * @throws Exception if an exception is thrown during the test
	 */
	@Test
	public void byuTest6_spacesDoNotMatter() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("BX 1665 .A1Z46 2002"));
		expectedOrder.add(new LCCallNumber("BX 1665 .A1 Z46 2002"));

		TestUtil.assertCorrectOrder(expectedOrder);
	}
	
	/**
	 * Callnums with additional cutters should sort after similar but simpler callnums  
	 * 
	 * @throws Exception if an exception is thrown during the test
	 */
	@Test
	public void byuTest7_additionalCutters() throws Exception {
		List<LCCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new LCCallNumber("F 681 .A78 1900"));
		expectedOrder.add(new LCCallNumber("F 681 .A78 Z12 1900"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}
	
}
