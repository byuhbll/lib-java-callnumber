package edu.byu.hbll.lib_java_callnum;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import edu.byu.hbll.callnumber.SymphonyDefaultCallNumber;

/**
 * Unit tests for SymphonyDefaultCallNumber.
 */
public class SymphonyDefaultCallNumberSortingTest {

	@Test
	public void byuTest1_wholeNumber() throws Exception {
		List<SymphonyDefaultCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new SymphonyDefaultCallNumber("XX(12345.1)"));
		expectedOrder.add(new SymphonyDefaultCallNumber("XX(67890.0)"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}

	@Test
	public void byuTest1_decimalNumber() throws Exception {
		List<SymphonyDefaultCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new SymphonyDefaultCallNumber("XX(12345.0)"));
		expectedOrder.add(new SymphonyDefaultCallNumber("XX(12345.1)"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}
	
}
