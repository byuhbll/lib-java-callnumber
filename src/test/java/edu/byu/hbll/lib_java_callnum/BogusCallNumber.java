package edu.byu.hbll.lib_java_callnum;

import edu.byu.hbll.callnumber.CallNumber;

/**
 * An implementation of CallNumber that deliberately does not define the constructor BogusCallNumber(String).
 * 
 * Attempting to use this implementation with CallNumberParser should result in an IllegalArgumentException.
 */
public class BogusCallNumber implements CallNumber {
	
	private static final long serialVersionUID = 1L;

	@Override
	public String sortKey() {
		return "";
	}

}
