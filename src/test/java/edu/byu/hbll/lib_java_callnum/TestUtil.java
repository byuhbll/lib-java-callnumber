package edu.byu.hbll.lib_java_callnum;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.byu.hbll.callnumber.CallNumber;

/**
 * Helper methods shared by multiple test classes.
 */
public class TestUtil {

	/**
	 * Asserts that the provided list's natural ordering matches its current ordering.
	 * 
	 * @param expectedOrder the list to order, already sorted into its expected ordering.
	 */
	public static void assertCorrectOrder(List<? extends CallNumber> expectedOrder) {
		List<CallNumber> actualOrder = new ArrayList<>(expectedOrder);
		Collections.shuffle(actualOrder);
		Collections.sort(actualOrder);		
		
		for(int i = 0; i < expectedOrder.size(); i++) {
			assertEquals(expectedOrder.get(i), actualOrder.get(i));
		}
	}

}
