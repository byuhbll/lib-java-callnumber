package edu.byu.hbll.lib_java_callnum;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import edu.byu.hbll.callnumber.UnclassifiedCallNumber;

/**
 * Unit tests for UnclassifiedCallNumber construction/parsing.
 */
public class UnclassifiedCallNumberTest {
	
	/**
	 * UnclassifiedCallNumber is unique amongst the provided implementations in that they can be constructed with a 
	 * null value.  If a null value is supplied to the constructor, it will be converted into an empty string and the
	 * empty string will be returned from both the toString and sortKey methods.
	 */
	@Test
	public void shouldConstructWithNullString() {
		UnclassifiedCallNumber callnum = new UnclassifiedCallNumber(null);
		
		assertEquals("", callnum.toString());
		assertEquals("", callnum.sortKey());
	}

	/**
	 * UnclassifiedCallNumber is unique amongst the provided implementations in that they can be constructed with an
	 * empty string.
	 */
	@Test
	public void shouldConstructWithEmptyString() {
		UnclassifiedCallNumber callnum = new UnclassifiedCallNumber("");
		
		assertEquals("", callnum.toString());
		assertEquals("", callnum.sortKey());
	}
	
	/**
	 * UnclassifiedCallNumber takes a minimalistic approach to normalizing and sorting provided values.  The toString
	 * method should return the trimmed version of the string, while the sortKey should additionally toLowerCase the
	 * provided value.  Sorting for UnclassifiedCallNumber is not tested further because it simply follows basic String
	 * ordering rules.
	 */
	@Test
	public void shouldConstructWithNonEmptyString() {
		UnclassifiedCallNumber callnum = new UnclassifiedCallNumber(" ABC 123 ");
		
		assertEquals("ABC 123", callnum.toString());
		assertEquals("abc 123", callnum.sortKey());
	}

	/**
	 * UnclassifiedCallNumber should order itself by using a trimmed, case-insensitive version of the original string.
	 */
	@Test
	public void shouldOrderAsTrimmedCaseInsensitiveString() {
		List<UnclassifiedCallNumber> expectedOrder = new ArrayList<>();
		expectedOrder.add(new UnclassifiedCallNumber("AbCdEfg"));
		expectedOrder.add(new UnclassifiedCallNumber("abcdefgh"));
		expectedOrder.add(new UnclassifiedCallNumber(" XX(12345.1)"));
		expectedOrder.add(new UnclassifiedCallNumber("XX(67890.0)"));
		
		TestUtil.assertCorrectOrder(expectedOrder);
	}


}
